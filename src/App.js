import Navbar from "./components/navbar/Navbar";
import Routes from "./routes/Router";
import "./app.css";
import { useContext, useEffect } from "react";
import UserContext from "./context/userContext";
import { useNavigate } from "react-router-dom";

function App() {
    const { data, storeData } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (!localStorage.getItem("flag")) {
            navigate("/login");
        }
    }, []);

    return (
        <div className="App">
            <Navbar />
            <Routes />
        </div>
    );
}

export default App;
