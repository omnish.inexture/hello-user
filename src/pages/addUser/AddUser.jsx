import React, { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import UserContext from "../../context/userContext";
import { addUser } from "../../redux/actions/userActions";
import style from "./AddUser.module.css";

const AddUser = () => {
    const { data } = useContext(UserContext);
    const [formData, setFormData] = useState({ name: "", job: "" });
    const dispatch = useDispatch();
    const navigate = useNavigate();

    // useEffect(() => {
    //     if (data.auth === false) {
    //         navigate("/login");
    //     }
    // }, [data]);

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(addUser(formData));
    };

    return (
        <div className={`container ${style.addUser}`}>
            <form className="form">
                <h1>Add User</h1>
                <input
                    type="text"
                    placeholder="name"
                    value={formData.name}
                    onChange={(e) =>
                        setFormData({ ...formData, name: e.target.value })
                    }
                />
                <input
                    type="text"
                    placeholder="job"
                    value={formData.job}
                    onChange={(e) =>
                        setFormData({ ...formData, job: e.target.value })
                    }
                />

                <button onClick={handleSubmit}>Submit</button>
            </form>
        </div>
    );
};

export default AddUser;
