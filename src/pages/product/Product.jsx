import React, { useEffect, useState } from "react";
import Loading from "../../assets/img/loading.gif";
import { useDispatch, useSelector } from "react-redux";
import Search from "../../components/product/Search";
import { getProducts } from "../../redux/actions/productActions";
import UseFilter from "../../hooks/useFilter";
import { useNavigate } from "react-router-dom";
import style from "./Product.module.css";

const Product = () => {
    const { products } = useSelector((state) => state);
    const dispatch = useDispatch();
    const [filterType, setFilterType] = useState("none");
    const [filterProduct, setFilterProduct] = useState([]);
    const [searchedDropDown, setSearchedDropDown] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(getProducts());
    }, [dispatch]);

    const getFilter = () => {
        const [filteredProduct] = UseFilter(products.products, filterType);
        setFilterProduct(filteredProduct);
    };

    useEffect(() => {
        getFilter();
    }, [filterType]);

    return products.status === "pending" ? (
        <div className="loadingContainer">
            <img src={Loading} alt="loading" />
        </div>
    ) : (
        <div className={`productContainer ${style.products}`}>
            <div className={style.search}>
                <Search
                    searchedDropDown={searchedDropDown}
                    setSearchedDropDown={setSearchedDropDown}
                />
            </div>

            <div className={style.filter}>
                <select onChange={(e) => setFilterType(e.target.value)}>
                    <option defaultValue value="none">
                        filter by
                    </option>
                    <option value="a-z">a-z</option>
                    <option value="z-a">z-a</option>
                    <option value="price low-high"> price low-high</option>
                    <option value="price high-low"> price high-low</option>
                </select>
            </div>

            <div className={style.productscontainer}>
                {products.products.length > 0 && filterProduct.length === 0
                    ? products.products.map((product) => (
                          <div
                              key={product.id}
                              onClick={() =>
                                  navigate(`/products/${product.id}`, {
                                      state: products.products,
                                  })
                              }
                          >
                              <img
                                  className="md-img"
                                  src={product.image}
                                  alt="product"
                              />
                              <h3>{product.title}</h3>
                              <p>
                                  {product.description.length > 200
                                      ? `${product.description.slice(
                                            0,
                                            200,
                                        )}...`
                                      : product.description}
                              </p>
                              <h4>₹{parseInt(product.price * 77.21)}</h4>
                          </div>
                      ))
                    : filterProduct.map((product) => (
                          <div
                              key={product.id}
                              onClick={() =>
                                  navigate(`/products/${product.id}`, {
                                      state: products.products,
                                  })
                              }
                          >
                              <img
                                  className="md-img"
                                  src={product.image}
                                  alt="product"
                              />
                              <h5>{product.title}</h5>
                              <p>
                                  {product.description.length > 200
                                      ? `${product.description.slice(
                                            0,
                                            200,
                                        )}...`
                                      : product.description}
                              </p>
                              <h4>₹{parseInt(product.price * 77.21)}</h4>
                          </div>
                      ))}
            </div>
        </div>
    );
};

export default Product;
