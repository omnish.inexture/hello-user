import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import style from "./Product.module.css";

const ProductDetail = () => {
    const { id } = useParams();
    const { products } = useSelector((state) => state);
    const { state } = useLocation();
    const [currentProduct, setCurrentProduct] = useState({});

    const navigate = useNavigate();

    useEffect(() => {
        setCurrentProduct(state.filter((product) => product.id === Number(id)));
    }, [id]);

    return (
        <div className={style.ProductDetailMainContainer}>
            <button onClick={() => navigate("/products")}>Back</button>
            {currentProduct.length > 0 ? (
                <div className={style.ProductDetail} key={id}>
                    <img
                        className="lg-img"
                        src={currentProduct[0].image}
                        alt="product"
                    />
                    <div>
                        <h2>{currentProduct[0].title}</h2>
                        <p>{currentProduct[0].description}</p>
                        <p>{currentProduct[0].category}</p>
                        <h3>₹{parseInt(currentProduct[0].price * 77.21)}</h3>
                    </div>
                </div>
            ) : (
                ""
            )}

            {state.length > 0 ? (
                <div className={style.productDetailWrapper}>
                    <h1>Releted Products</h1>
                    <div className={style.productDetailcontainer}>
                        {state.map((product) =>
                            product.category === currentProduct[0]?.category &&
                            currentProduct[0]?.id !== product.id ? (
                                <div
                                    key={product.id}
                                    onClick={() =>
                                        navigate(`/products/${product.id}`, {
                                            state: products.products,
                                        })
                                    }
                                >
                                    <img
                                        className="md-img"
                                        src={product.image}
                                        alt="product"
                                    />
                                    <p>{product.title}</p>
                                    <h5>₹{parseInt(product.price * 77.21)}</h5>
                                </div>
                            ) : (
                                ""
                            ),
                        )}
                    </div>
                </div>
            ) : (
                ""
            )}
        </div>
    );
};

export default ProductDetail;
