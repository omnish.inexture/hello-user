import React, { useContext, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import UserContext from "../context/userContext";

const Login = () => {
    const [formData, setFormData] = useState({ email: "", password: "" });
    const navigate = useNavigate();
    const { storeData } = useContext(UserContext);

    const handleSubmit = (e) => {
        e.preventDefault();
        const localData = JSON.parse(localStorage.getItem("data"));

        if (
            localData.email === formData.email &&
            localData.password === formData.password
        ) {
            storeData({ email: formData.email, auth: true });
            localStorage.setItem("flag", true);
            navigate("/");
        } else {
            alert("invalide credentials");
        }
    };

    return (
        <div className="container">
            <form className="form">
                <h1>Login</h1>
                <input
                    type="email"
                    placeholder="email"
                    value={formData.email}
                    onChange={(e) =>
                        setFormData({ ...formData, email: e.target.value })
                    }
                />
                <input
                    type="password"
                    placeholder="password"
                    value={formData.password}
                    onChange={(e) =>
                        setFormData({ ...formData, password: e.target.value })
                    }
                />

                <button
                    disabled={formData.email === "" || formData.password === ""}
                    onClick={(e) => handleSubmit(e)}
                >
                    Submit
                </button>
            </form>
        </div>
    );
};

export default Login;
