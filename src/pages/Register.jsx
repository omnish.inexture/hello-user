import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "../context/userContext";

const Register = () => {
    const [formData, setFormData] = useState({ email: "", password: "" });
    const { data } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (data.auth === true) {
            navigate("/");
        }
    }, [data]);

    const handleSubmit = (e) => {
        e.preventDefault();
        localStorage.setItem("data", JSON.stringify(formData));
        setFormData({ email: "", password: "" });
    };

    return (
        <div className="container">
            <form className="form">
                <h1>Register</h1>
                <input
                    type="email"
                    placeholder="email"
                    value={formData.email}
                    onChange={(e) =>
                        setFormData({ ...formData, email: e.target.value })
                    }
                />
                <input
                    type="password"
                    placeholder="password"
                    value={formData.password}
                    onChange={(e) =>
                        setFormData({ ...formData, password: e.target.value })
                    }
                />

                <button
                    disabled={formData.email === "" || formData.password === ""}
                    onClick={(e) => handleSubmit(e)}
                >
                    Submit
                </button>
            </form>
        </div>
    );
};

export default Register;
