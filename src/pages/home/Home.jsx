import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getUsers } from "../../redux/actions/userActions";
import style from "./Home.module.css";
import Loading from "../../assets/img/loading.gif";

const Home = () => {
    const dispatch = useDispatch();
    const { users, status } = useSelector((state) => state.users);

    useEffect(() => {
        dispatch(getUsers());
    }, []);

    return (
        <div className={`container ${style.home}`}>
            {status === "pending" ? (
                <img src={Loading} alt="loading" />
            ) : (
                <div className={style.boxContainer}>
                    {users.map((user) => (
                        <Link
                            className={style.box}
                            to={`users/${user.id}`}
                            key={user.id}
                        >
                            <img src={user.avatar} alt="user avatar" />
                            <div className={style.info}>
                                <p>{user.first_name}</p>
                                <p>{user.last_name}</p>
                                <p>{user.email}</p>
                            </div>
                        </Link>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Home;
