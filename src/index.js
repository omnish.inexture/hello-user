import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import { Provider } from "react-redux";
import store from "./redux/store";
import UserState from "./context/userState";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <UserState>
                <BrowserRouter>
                    <App />
                </BrowserRouter>
            </UserState>
        </Provider>
    </React.StrictMode>,
);
