import { useRoutes } from "react-router-dom";
import Home from "../pages/home/Home";
import SingleUser from "../components/singleUser/SingleUser";
import AddUser from "../pages/addUser/AddUser";
import Register from "../pages/Register";
import Login from "../pages/Login";
import Product from "../pages/product/Product";
import ProductDetail from "../pages/product/ProductDetail";

const Routes = () => {
    const routes = useRoutes([
        { path: "/", element: <Home /> },
        { path: "/users/:id", element: <SingleUser /> },
        { path: "/addUser", element: <AddUser /> },
        { path: "/products", element: <Product /> },
        { path: "/products/:id", element: <ProductDetail /> },
        { path: "/register", element: <Register /> },
        { path: "/login", element: <Login /> },
    ]);

    return routes;
};

export default Routes;
