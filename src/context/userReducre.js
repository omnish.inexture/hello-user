const userReducer = (state, action) => {
    switch (action.type) {
        case "STORE_DATA":
            return {
                ...state,
                data: action.payload.email,
                auth: action.payload.auth,
            };

        default:
            return state;
    }
};

export default userReducer;
