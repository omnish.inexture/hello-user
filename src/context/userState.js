import React, { useReducer } from "react";
import UserReducer from "./userReducre";
import UserContext from "./userContext";

const DataState = (props) => {
    const initial = {
        auth: false,
        email: "",
    };

    const [state, dispatch] = useReducer(UserReducer, initial);

    const storeData = async (data) => {
        dispatch({
            type: "STORE_DATA",
            payload: data,
        });
    };

    return (
        <UserContext.Provider value={{ data: state, storeData }}>
            {props.children}
        </UserContext.Provider>
    );
};

export default DataState;
