import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import style from "../../pages/product/Product.module.css";

const Search = ({ searchedDropDown, setSearchedDropDown }) => {
    const [search, setsetSearch] = useState("");

    const { products } = useSelector((state) => state);
    const inpRef = useRef();
    const navigate = useNavigate();

    const handleSearch = () => {
        setsetSearch(inpRef.current.value);
    };

    useEffect(() => {
        if (search !== "" && search.length >= 3) {
            const arr = products.products.filter((item) =>
                item.title.toLowerCase().includes(search.toLowerCase()),
            );
            setSearchedDropDown(arr);
        } else {
            setSearchedDropDown([]);
        }
    }, [search, products.products]);

    

    const getHighlightedText = (text, highlight) => {
        const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
        console.log(parts);
        return <span> { parts.map((part, i) => 
            <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' } : {} }>
                { part }
            </span>)
        } </span>;
    }

    

    return (
        <div className={style.searchContainer}>
            <input
                type="text"
                placeholder="Search products here..."
                value={search}
                ref={inpRef}
                onChange={handleSearch}
            />
            {searchedDropDown.length > 0 && (
                <div className={style.searchDropdownContainer}>
                    {searchedDropDown.length > 0
                        ? searchedDropDown.map((product) => (
                              <div
                                  className={style.searchDropdownProduct}
                                  onClick={() =>
                                      navigate(`/products/${product.id}`, {
                                          state: products.products,
                                      })
                                  }
                                  key={product.id}
                              >
                                  <img
                                      className="sm-img"
                                      src={product.image}
                                      alt="product"
                                  />
                                  <p>
                                      {product?.title
                                          .split("", /search/i)
                                          .join(search)}
                                  </p>
                                  <div>
                                        
                                      <small>
                                        {getHighlightedText(product.title, search)}
                                      </small>
                                  </div>
                                  <small>
                                      ₹{parseInt(product.price * 77.21)}
                                  </small>
                              </div>
                          ))
                        : search.length > 3 && <p>No product found</p>}
                </div>
            )}
        </div>
    );
};

export default Search;
