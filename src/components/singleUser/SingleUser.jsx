import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { updateUser } from "../../redux/actions/userActions";
import style from "./SingleUser.module.css";

const SingleUser = () => {
    const { id } = useParams();
    const { users } = useSelector((state) => state.users);
    const [user, setuser] = useState({});
    const [formData, setFormData] = useState({ name: "", job: "" });
    const [modal, setModal] = useState(false);
    const dispatch = useDispatch();

    const navigate = useNavigate();

    useEffect(() => {
        if (users.length === 0) {
            navigate("/");
        }
        users.map((user) => (user.id === Number(id) ? setuser(user) : ""));
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(updateUser(id, formData));
        setFormData({ name: "", job: "" });
        setModal(false);
    };

    const stopPropogation = (e) => {
        e.stopPropagation();
    };

    return (
        <div className={`container ${style.singleUser}`}>
            {user && (
                <div className={style.singleUserCard}>
                    <div className={style.imgContainer}>
                        <img src={user.avatar} alt="user avatar" />
                    </div>

                    <div>
                        <p>{user.first_name}</p>
                        <p>{user.last_name}</p>
                        <p>{user.email}</p>
                    </div>
                </div>
            )}

            <div>
                <button
                    className={style.updateUser}
                    onClick={() => setModal(true)}
                >
                    Update User
                </button>
            </div>
            {modal && (
                <div
                    onClick={() => setModal(false)}
                    className={style.updateUserFormContainer}
                >
                    <form onClick={stopPropogation}>
                        <div onClick={() => setModal(false)}>&times;</div>
                        <input
                            type="text"
                            placeholder="name"
                            value={formData.name}
                            onChange={(e) =>
                                setFormData({
                                    ...formData,
                                    name: e.target.value,
                                })
                            }
                        />
                        <input
                            type="text"
                            placeholder="job"
                            value={formData.job}
                            onChange={(e) =>
                                setFormData({
                                    ...formData,
                                    job: e.target.value,
                                })
                            }
                        />

                        <button onClick={handleSubmit}>Submit</button>
                    </form>
                </div>
            )}
        </div>
    );
};

export default SingleUser;
