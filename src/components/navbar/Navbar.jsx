import React, { useContext } from "react";
import { Link } from "react-router-dom";
import UserContext from "../../context/userContext";
import style from "./Navbar.module.css";

const Navbar = () => {
    const { storeData } = useContext(UserContext);
    const handleLogout = () => {
        storeData({ email: "", auth: false });
        localStorage.setItem("flag", false);
    };

    return (
        <div className={style.navbar}>
            <ul>
                {localStorage.getItem("flag") === "true" && (
                    <>
                        <li>
                            <Link to="/">Users</Link>
                        </li>
                        <li>
                            <Link to="/addUser">Add User</Link>
                        </li>
                        <li>
                            <Link to="/products">Products</Link>
                        </li>
                        <li>
                            <button onClick={handleLogout} to="/logout">
                                Logout
                            </button>
                        </li>
                    </>
                )}

                {localStorage.getItem("flag") === "false" ||
                    (localStorage.getItem("flag") === null && (
                        <>
                            <li>
                                <Link to="/register">Register</Link>
                            </li>
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                        </>
                    ))}
            </ul>
        </div>
    );
};

export default Navbar;
