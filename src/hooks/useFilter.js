import { useSelector } from "react-redux";

const UseFilter = (product, filterType) => {
    let filteredProduct = [...product];
    switch (filterType) {
        case "none":
            break;

        case "a-z":
            filteredProduct.sort((a, b) => (a.title > b.title ? 1 : -1));
            break;

        case "z-a":
            filteredProduct.sort((a, b) => (a.title > b.title ? -1 : 1));
            break;

        case "price low-high":
            filteredProduct.sort((a, b) => (a.price > b.price ? 1 : -1));
            break;

        case "price high-low":
            filteredProduct.sort((a, b) => (a.price > b.price ? -1 : 1));
            break;

        default:
            break;
    }

    return [filteredProduct];
};

export default UseFilter;
