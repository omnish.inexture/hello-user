// import { useEffect, useState } from "react";
// import axios from "axios";

// const UseFetch = (url, method) => {
//     const [data, setData] = useState([]);

//     useEffect(() => {
//         let fetchData;
//         switch (method) {
//             case "GET":
//                 fetchData = async () => {
//                     const res = await axios.get(url);
//                     const data = await res.json();
//                     setData(data);
//                 };
//                 fetchData();

//             default
//              return null
//         }
//     }, [url]);

//     return [data];
// };

// export default UseFetch;
