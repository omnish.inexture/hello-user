import { getRequest, postRequest, putRequest } from "../../utils/fetchData";
import {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_ERROR,
    UPDATE_USER_ERROR,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_REQUEST,
    ADD_USER_REQUEST,
    ADD_USER_SUCCESS,
    ADD_USER_ERROR,
} from "../types";

export const getUsers = () => async (dispatch) => {
    try {
        dispatch({ type: GET_USER_REQUEST });

        const res = await getRequest("https://reqres.in/api/users");

        dispatch({ type: GET_USER_SUCCESS, payload: res.data.data });
    } catch (error) {
        dispatch({ type: GET_USER_ERROR, payload: error.message });
    }
};

export const updateUser = (id, data) => async (dispatch) => {
    try {
        dispatch({ type: UPDATE_USER_REQUEST });

        const res = await putRequest(`/users/${id}`, data);

        dispatch({ type: UPDATE_USER_SUCCESS, payload: res.data });
    } catch (error) {
        dispatch({ type: UPDATE_USER_ERROR, payload: error.message });
    }
};

export const addUser = (data) => async (dispatch) => {
    try {
        dispatch({ type: ADD_USER_REQUEST });

        const res = await postRequest(`/users`, data);

        dispatch({ type: ADD_USER_SUCCESS, payload: res.data });
    } catch (error) {
        dispatch({ type: ADD_USER_ERROR, payload: error.message });
    }
};
