import { getRequest } from "../../utils/fetchData";
import {
    GET_PRODUCT_REQUEST,
    GET_PRODUCT_SUCCESS,
    GET_PRODUCT_ERROR,
} from "../types";

export const getProducts = () => async (dispatch) => {
    try {
        dispatch({ type: GET_PRODUCT_REQUEST });

        const res = await getRequest("https://fakestoreapi.com/products");

        dispatch({ type: GET_PRODUCT_SUCCESS, payload: res.data });
    } catch (error) {
        dispatch({ type: GET_PRODUCT_ERROR, payload: error.message });
    }
};
