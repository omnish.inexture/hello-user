import {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_ERROR,
    UPDATE_USER_ERROR,
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,
    ADD_USER_ERROR,
    ADD_USER_REQUEST,
    ADD_USER_SUCCESS,
} from "../types";

const initalState = {
    status: null,
    users: [],
    user: {},
    message: "",
};

const userReducer = (state = initalState, action) => {
    switch (action.type) {
        case GET_USER_REQUEST:
            return { ...state, status: "pending" };

        case GET_USER_SUCCESS:
            return {
                ...state,
                status: "success",
                users: action.payload,
                message: "Got Data successfully",
            };

        case GET_USER_ERROR:
            return {
                ...state,
                status: "error",
                users: [],
                message: action.payload,
            };

        case UPDATE_USER_REQUEST:
            return { ...state, status: "pending" };

        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                status: "success",
                message: "Update user successfully",
            };

        case UPDATE_USER_ERROR:
            return {
                ...state,
                status: "error",
                message: action.payload,
            };

        case ADD_USER_REQUEST:
            return { ...state, status: "pending" };

        case ADD_USER_SUCCESS:
            return {
                ...state,
                status: "success",
                user: action.payload,
                message: "Add user successfully",
            };

        case ADD_USER_ERROR:
            return {
                ...state,
                status: "error",
                user: [],
                message: action.payload,
            };

        default:
            return state;
    }
};

export default userReducer;
