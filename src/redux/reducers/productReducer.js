import {
    GET_PRODUCT_REQUEST,
    GET_PRODUCT_SUCCESS,
    GET_PRODUCT_ERROR,
} from "../types";

const initalState = {
    status: null,
    products: [],
    message: "",
};

const productReducer = (state = initalState, action) => {
    switch (action.type) {
        case GET_PRODUCT_REQUEST:
            return { ...state, status: "pending" };

        case GET_PRODUCT_SUCCESS:
            return {
                ...state,
                status: "success",
                products: action.payload,
                message: "Got Data successfully",
            };

        case GET_PRODUCT_ERROR:
            return {
                ...state,
                status: "error",
                users: [],
                message: action.payload,
            };

        default:
            return state;
    }
};

export default productReducer;
