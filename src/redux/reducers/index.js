import { combineReducers } from "redux";
import users from "./userReducer";
import products from "./productReducer";

const rootReducer = combineReducers({
    users,
    products
});

export default rootReducer;
